import tensorflow as tf

import sys
import os.path
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.path.pardir)))

from re3_utils.tensorflow_util import tf_util
from re3_utils.tensorflow_util.CaffeLSTMCell import CaffeLSTMCell

from constants import LSTM_SIZE, RES_LSTM_SIZE, DENSE_LSTM_SIZE

IMAGENET_MEAN = [123.151630838, 115.902882574, 103.062623801]

msra_initializer = tf.contrib.layers.variance_scaling_initializer()
bias_initializer = tf.zeros_initializer()
prelu_initializer = tf.constant_initializer(0.25)

# creates the nodes necessary for the AlexNet network
# Parameters:
# fixCnn: boolean [False], whether to fix the wights of the cnn or not
def alexnet_conv_layers(input, batch_size, num_unrolls, trainable=True):
    input = tf.to_float(input) - IMAGENET_MEAN
    with tf.variable_scope('conv1'):
        conv1 = tf_util.conv_layer(input, 96, 11, 4, padding='VALID', trainable=trainable)
        pool1 = tf.nn.max_pool(
                conv1, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='VALID',
                name='pool1')
        lrn1 = tf.nn.local_response_normalization(pool1, depth_radius=2,
                alpha=2e-5, beta=0.75, bias=1.0, name='norm1')

    with tf.variable_scope('conv1_skip'):
        prelu_skip = tf_util.get_variable('prelu', shape=[16], dtype=tf.float32,
                initializer=prelu_initializer)

        conv1_skip = tf_util.prelu(tf_util.conv_layer(lrn1, 16, 1, activation=None),
                prelu_skip)
        conv1_skip = tf.transpose(conv1_skip, perm=[0,3,1,2])
        conv1_skip_flat = tf_util.remove_axis(conv1_skip, [2,3])

    with tf.variable_scope('conv2'):
        conv2 = tf_util.conv_layer(lrn1, 256, 5, num_groups=2, padding='SAME', trainable=trainable)
        pool2 = tf.nn.max_pool(
                conv2, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='VALID',
                name='pool2')
        lrn2 = tf.nn.local_response_normalization(pool2, depth_radius=2,
                alpha=2e-5, beta=0.75, bias=1.0, name='norm2')

    with tf.variable_scope('conv2_skip'):
        prelu_skip = tf_util.get_variable('prelu', shape=[32], dtype=tf.float32,
                initializer=prelu_initializer)

        conv2_skip = tf_util.prelu(tf_util.conv_layer(lrn2, 32, 1, activation=None),
                prelu_skip)
        conv2_skip = tf.transpose(conv2_skip, perm=[0,3,1,2])
        conv2_skip_flat = tf_util.remove_axis(conv2_skip, [2,3])

    with tf.variable_scope('conv3'):
        conv3 = tf_util.conv_layer(lrn2, 384, 3, padding='SAME', trainable=trainable)

    with tf.variable_scope('conv4'):
        conv4 = tf_util.conv_layer(conv3, 384, 3, num_groups=2, padding='SAME', trainable=trainable)

    with tf.variable_scope('conv5'):
        conv5 = tf_util.conv_layer(conv4, 256, 3, num_groups=2, padding='SAME', trainable=trainable)
        pool5 = tf.nn.max_pool(
                conv5, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1], padding='VALID',
                name='pool5')
        pool5 = tf.transpose(pool5, perm=[0,3,1,2])
        pool5_flat = tf_util.remove_axis(pool5, [2,3])

    with tf.variable_scope('conv5_skip'):
        prelu_skip = tf_util.get_variable('prelu', shape=[64], dtype=tf.float32,
                initializer=prelu_initializer)

        conv5_skip = tf_util.prelu(tf_util.conv_layer(conv5, 64, 1, activation=None),
                prelu_skip)
        conv5_skip = tf.transpose(conv5_skip, perm=[0,3,1,2])
        conv5_skip_flat = tf_util.remove_axis(conv5_skip, [2,3])

    with tf.variable_scope('big_concat'):
        # Concat all skip layers.
        skip_concat = tf.concat([conv1_skip_flat, conv2_skip_flat, conv5_skip_flat, pool5_flat], 1)
        skip_concat_shape = skip_concat.get_shape().as_list()

        # Split and merge image pairs
        # (BxTx2)xHxWxC
        pool5_reshape = tf.reshape(skip_concat, [batch_size, num_unrolls, 2, skip_concat_shape[-1]])
        # (BxT)x(2xHxWxC)
        reshaped = tf_util.remove_axis(pool5_reshape, [1,3])

        return reshaped

# fixCnn: boolean, whether to fix the cnn weights or not. Default: False
def inference(inputs, num_unrolls, train, batch_size=None, prevState=None, reuse=None, trainCnn=True, useGru=False, useResLstm=False, useLayerNormLstm=False, useCudnnLstm=False, viewActivations=False, useDenseLstm=False):
    # Data should be in order BxTx2xHxWxC where T is the number of unrolls
    # Mean subtraction
    if batch_size is None:
        batch_size = int(inputs.get_shape().as_list()[0] / (num_unrolls * 2))

    variable_list = []

    if reuse is not None and not reuse:
        reuse = None

    with tf.variable_scope('re3', reuse=reuse):
        conv_layers = alexnet_conv_layers(inputs, batch_size, num_unrolls, trainable=trainCnn)

        # Embed Fully Connected Layer
        with tf.variable_scope('fc6'):

            # add fc layer
            fcSize = 1024
            if useResLstm:
                fcSize = RES_LSTM_SIZE
            elif useDenseLstm:
                fcSize = 900
            fc6_out = tf_util.fc_layer(conv_layers, fcSize)

            # add batchNorm if we are using resLstm
            if useResLstm or useDenseLstm:
                fc6_out = tf.layers.batch_normalization(fc6_out, training=train)

            # (BxT)xC
            fc6_reshape = tf.reshape(fc6_out, tf.stack([batch_size, num_unrolls, fc6_out.get_shape().as_list()[-1]]))

        # will contain states of each lstm
        lstmStates = []
        lstmOuts = []
        # LSTM
        if not useResLstm and not useGru and not useDenseLstm:
            swap_memory = num_unrolls > 1
            with tf.variable_scope('lstm1'):

                lstm1 = tf.contrib.rnn.LSTMCell(LSTM_SIZE, use_peepholes=True, initializer=msra_initializer, reuse=reuse)

                if prevState is not None:
                    state1 = tf.contrib.rnn.LSTMStateTuple(prevState[0], prevState[1])
                else:
                    state1 = lstm1.zero_state(batch_size, dtype=tf.float32)
                lstm1_outputs, state1 = tf.nn.dynamic_rnn(lstm1, fc6_reshape, initial_state=state1, swap_memory=swap_memory)

                lstmStates.append(state1)

                # show training progress on tensorboard
                if train:
                    lstmVars = [var for var in tf.trainable_variables() if 'lstm1' in var.name]
                    for var in lstmVars:
                        tf_util.variable_summaries(var, var.name[:-2])

            with tf.variable_scope('lstm2'):

                lstm2 = tf.contrib.rnn.LSTMCell(LSTM_SIZE, use_peepholes=True, initializer=msra_initializer, reuse=reuse)
                if prevState is not None:
                    state2 = tf.contrib.rnn.LSTMStateTuple(prevState[2], prevState[3])
                else:
                    state2 = lstm2.zero_state(batch_size, dtype=tf.float32)
                lstm2_inputs = tf.concat([fc6_reshape, lstm1_outputs], 2)
                lstm2_outputs, state2 = tf.nn.dynamic_rnn(lstm2, lstm2_inputs, initial_state=state2, swap_memory=swap_memory)

                lstmStates.append(state2)

                # show training progress on tensorboard
                if train:
                    lstmVars = [var for var in tf.trainable_variables() if 'lstm2' in var.name]
                    for var in lstmVars:
                        tf_util.variable_summaries(var, var.name[:-2])
                # (BxT)xC
                outputs_reshape = tf_util.remove_axis(lstm2_outputs, 1)

        # residual LSTM
        elif useResLstm:
            swap_memory = num_unrolls > 1

            # creates a residual block of lstm
            # params:
            #   numBlocks: number of residual blocks
            #   numUnits: number of lstm units to use per block
            def resLstmBlock(input, numBlocks, numUnits):

                for i in range(numBlocks):
                    blockName = 'resBlock' + str(i)
                    with tf.variable_scope(blockName):

                        lstmOut = lstmBlock(input, i, numUnits)
                        lstmOuts.append(lstmOut)
                        input = input + lstmOut

                return input, lstmOuts

            # creates a block of lstm to later create a residual lstm network
            # params:
            #   blockNo: number of the block(call the function with progressive blockNo numbers, from 0...N)
            #   numUnits: number of lstm units to use per block
            def lstmBlock(input, blockNo, numUnits):

                prevLstmOutput = None

                # iterate along the lstm units of a block
                for i in range(numUnits):

                    lstmId = 2*blockNo + i
                    lstmName = 'lstm' + str(lstmId)
                    #print lstmName

                    with tf.variable_scope(lstmName):

                        # layerNormLstm
                        if useLayerNormLstm:
                            lstm = tf.contrib.rnn.LayerNormBasicLSTMCell(RES_LSTM_SIZE, reuse=reuse)
                        # cudnnLstm
                        elif useCudnnLstm:
                            lstm = tf.contrib.cudnn_rnn.CudnnCompatibleLSTMCell(RES_LSTM_SIZE, reuse=reuse)
                        # default lstm
                        else:
                            lstm = tf.contrib.rnn.LSTMCell(RES_LSTM_SIZE, use_peepholes=True, initializer=msra_initializer, reuse=reuse)

                        if prevState is not None:
                            state = tf.contrib.rnn.LSTMStateTuple(prevState[2*lstmId], prevState[2*lstmId + 1])
                        else:
                            state = lstm.zero_state(batch_size, dtype=tf.float32)

                        lstm_output, state = tf.nn.dynamic_rnn(lstm, input, initial_state=state,
                                                                  swap_memory=swap_memory)
                        lstmStates.append(state)
                        input = lstm_output

                # return the output of the last lstm unit
                return input

            output, lstmOuts = resLstmBlock(fc6_reshape, numBlocks=3, numUnits=2)
            outputs_reshape = tf_util.remove_axis(output, 1)

        # GRU TODO fix
        elif useGru:
            swap_memory = num_unrolls > 1
            with tf.variable_scope('gru1'):

                gru1 = tf.contrib.rnn.GRUCell(LSTM_SIZE, kernel_initializer=msra_initializer,
                                               bias_initializer=msra_initializer, reuse=reuse)
                if prevState is not None:
                    state1 = prevState[0]
                else:
                    state1 = gru1.zero_state(batch_size, dtype=tf.float32)
                gru1_outputs, state1 = tf.nn.dynamic_rnn(gru1, fc6_reshape, initial_state=state1,
                                                          swap_memory=swap_memory)

                lstmStates.append(state1)

                # visualize variables on tensorboard if training
                if train:
                    gruVars = [var for var in tf.trainable_variables() if 'gru1' in var.name]
                    for var in gruVars:
                        tf_util.variable_summaries(var, var.name[:-2])

            with tf.variable_scope('gru2'):

                gru2 = tf.contrib.rnn.GRUCell(LSTM_SIZE, kernel_initializer=msra_initializer,
                                              bias_initializer=msra_initializer, reuse=reuse)

                if prevState is not None:
                    state2 = prevState[1]
                else:
                    state2 = gru2.zero_state(batch_size, dtype=tf.float32)
                gru2_inputs = tf.concat([fc6_reshape, gru1_outputs], 2)
                gru2_outputs, state2 = tf.nn.dynamic_rnn(gru2, gru2_inputs, initial_state=state2,
                                                          swap_memory=swap_memory)

                lstmStates.append(state2)

                # visualize vars on tensorboard if training
                if train:
                    gruVars = [var for var in tf.trainable_variables() if 'gru2' in var.name]
                    for var in gruVars:
                        tf_util.variable_summaries(var, var.name[:-2])

                # (BxT)xC
                outputs_reshape = tf_util.remove_axis(gru2_outputs, 1)

        # Dense LSTM
        elif useDenseLstm:
            swap_memory = num_unrolls > 1

            for i in range(4):

                lstmName = 'lstm' + str(i)
                with tf.variable_scope(lstmName):

                    lstm = tf.contrib.rnn.LayerNormBasicLSTMCell(DENSE_LSTM_SIZE, reuse=reuse)

                    if prevState is not None:
                       state = tf.contrib.rnn.LSTMStateTuple(prevState[2*i], prevState[2*i + 1])
                    else:
                        state = lstm.zero_state(batch_size, dtype=tf.float32)

                    lstm_inputs = [fc6_reshape] + lstmOuts
                    lstm_inputs = tf.concat(lstm_inputs, -1)

                    lstm_outputs, state = tf.nn.dynamic_rnn(lstm, lstm_inputs, initial_state=state, swap_memory=swap_memory)

                    lstmStates.append(state)
                    lstmOuts.append(lstm_outputs)

                    # show training progress on tensorboard
                    if train:
                        lstmVars = [var for var in tf.trainable_variables() if lstmName in var.name]
                        for var in lstmVars:
                            tf_util.variable_summaries(var, var.name[:-2])

            outputs_reshape = tf_util.remove_axis(lstmOuts[-1], 1)

        # Final FC layer.
        with tf.variable_scope('fc_output'):
            fc_output_out = tf_util.fc_layer(outputs_reshape, 4, activation=None)

    if viewActivations:
        if prevState is not None:
            return fc_output_out, fc6_reshape, lstmOuts, lstmStates
        else:
            return fc_output_out, fc6_reshape, lstmOuts
    else:
        if prevState is not None:
            return fc_output_out, lstmStates
        else:
            return fc_output_out

# returns the list of variables used for training
def get_var_list():
    return tf.trainable_variables()

def loss(outputs, labels):
    with tf.variable_scope('loss'):

        #loss = tf.losses.huber_loss(labels, outputs)

        # original loss
        diff = tf.reduce_sum(tf.abs(outputs - labels, name='diff'), axis=1)
        maeLoss = tf.reduce_mean(diff, name='loss')
        loss = maeLoss

    # L2 Loss on variables.
    with tf.variable_scope('l2_weight_penalty'):
        l2_weight_penalty = 0.0005 * tf.add_n([tf.nn.l2_loss(v)
            for v in get_var_list()])

    full_loss = loss + l2_weight_penalty

    return full_loss, loss, maeLoss

def training(loss, learning_rate):

    # experimental
    '''cnnVarsNames = []
    for i in range(5):
        cnnLayerName = 're3/conv' + str(i + 1)
        wConvName = cnnLayerName + '/W_conv:0'
        bConvName = cnnLayerName + '/b_conv:0'
        cnnVarsNames.append(wConvName)
        cnnVarsNames.append(bConvName)

    cnnVars = []
    otherVars = []
    for var in tf.trainable_variables():

        if var.name in cnnVarsNames:
            cnnVars.append(var)
        else:
            otherVars.append(var)

    # debug
    print 'CnnVars:'
    for var in cnnVars:
        print var.name
    print '\n'

    print 'otherVars:'
    for var in otherVars:
        print var.name
    print '\n'

    var_list1 = cnnVars
    var_list2 = otherVars
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    opt1 = tf.train.AdamOptimizer(learning_rate=learning_rate*0.1)
    opt2 = tf.train.AdamOptimizer(learning_rate=learning_rate)
    with tf.control_dependencies(update_ops):
        grads = tf.gradients(loss, var_list1 + var_list2)
        grads1 = grads[:len(var_list1)]
        grads2 = grads[len(var_list1):]
        train_op1 = opt1.apply_gradients(zip(grads1, var_list1))
        train_op2 = opt2.apply_gradients(zip(grads2, var_list2))
        train_op = tf.group(train_op1, train_op2)
    return train_op'''

    # original
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
    with tf.device('/cpu:0'):
        global_step = tf.train.create_global_step()

    # visualize gradients during loss calculation
    #train_op = tf.contrib.layers.optimize_loss(loss, global_step, optimizer='Adam', learning_rate=learning_rate,
    #                                           variables=get_var_list(), summaries=['gradients'], colocate_gradients_with_ops=True)
    # original version of train_op
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    with tf.control_dependencies(update_ops):
    	train_op = optimizer.minimize(loss, var_list=get_var_list(), global_step=global_step, colocate_gradients_with_ops=True)

    return train_op

