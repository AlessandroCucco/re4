import pdb
import argparse
import cv2
import glob
import numpy as np
import os
import random
import struct
import sys

import time
import threading

# print detailed stack trace
#import faulthandler
#faulthandler.enable()

from io import BytesIO

import sys
import os.path
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(__file__), os.path.pardir)))

import tf_dataset
import test_net
from tracker import network
from tracker import re3_tracker
from re3_utils.util import bb_util
from re3_utils.util import im_util
from re3_utils.tensorflow_util import tf_util
from re3_utils.util import drawing
from re3_utils.util import IOU


from constants import CROP_PAD
from constants import CROP_SIZE
from constants import LSTM_SIZE
from constants import RES_LSTM_SIZE
from constants import DENSE_LSTM_SIZE
from constants import GPU_ID
from constants import LOG_DIR
from constants import OUTPUT_WIDTH
from constants import OUTPUT_HEIGHT

import tensorflow as tf

HOST = 'localhost'
NUM_ITERATIONS = 300002#int(1e6)
PORT = 9997

#LEARNING_RATE = 1e-5

def main(FLAGS):
    global PORT, delta, REPLAY_BUFFER_SIZE
    delta = FLAGS.delta
    batchSize = FLAGS.batch_size
    timing = FLAGS.timing
    debug = FLAGS.debug or FLAGS.output
    PORT = FLAGS.port

    # added by Alessandro
    trainCnn = not FLAGS.fix_cnn # run backprop on cnn, too
    useGru   = FLAGS.use_gru     # use GRU instead of LSTM
    learning_rate = FLAGS.learning_rate
    useNetworkProb = FLAGS.network_prob
    useResLstm = FLAGS.res_lstm
    useLayerNormLstm = FLAGS.layer_norm_lstm
    useCudnnLstm = FLAGS.cudnn_lstm
    useDenseLstm = FLAGS.dense_lstm

    print 'TrainCnn: ', trainCnn
    print 'UseGru: ', useGru
    print 'LearningRate: ', learning_rate
    print 'UseNetworkProb: ', useNetworkProb
    print 'ResLstm: ', useResLstm
    print 'LayerNormLstm: ', useLayerNormLstm
    print 'CudnnLstm: ', useCudnnLstm
    print 'DenseLstm: ', useDenseLstm

    os.environ['CUDA_VISIBLE_DEVICES'] = str(FLAGS.cuda_visible_devices)
    np.set_printoptions(suppress=True)
    np.set_printoptions(precision=4)

    # Tensorflow setup
    if not os.path.exists(LOG_DIR):
        os.makedirs(LOG_DIR)
    if not os.path.exists(LOG_DIR + '/checkpoints'):
        os.makedirs(LOG_DIR + '/checkpoints')

    tf.Graph().as_default()
    tf.logging.set_verbosity(tf.logging.INFO)

    sess = tf_util.Session()

    # Create the nodes for single image forward passes for learning to fix mistakes.
    # Parameters here are shared with the learned network.
    if ',' in FLAGS.cuda_visible_devices:
        with tf.device('/gpu:1'):
            forwardNetworkImagePlaceholder = tf.placeholder(tf.uint8, shape=(2, CROP_SIZE, CROP_SIZE, 3))

            # GRU
            if useGru:
                prevState = tuple([tf.placeholder(tf.float32, shape=(1, LSTM_SIZE)) for _ in range(2)])
            # res LSTM
            elif useResLstm:
                prevState = tuple([tf.placeholder(tf.float32, shape=(1, RES_LSTM_SIZE)) for _ in range(12)])
            # dense LSTM
            elif useDenseLstm:
                prevState = tuple([tf.placeholder(tf.float32, shape=(1, DENSE_LSTM_SIZE)) for _ in range(8)])
            # re3
            else:
                prevState = tuple([tf.placeholder(tf.float32, shape=(1, LSTM_SIZE)) for _ in range(4)])

            networkOutputs, fc6, lstmOuts, lstmStates = network.inference(  #state1, state2 = network.inference(
                    forwardNetworkImagePlaceholder, num_unrolls=1, train=False,
                    prevState=prevState, reuse=False, trainCnn=trainCnn, useGru=useGru, useResLstm=useResLstm, useLayerNormLstm=useLayerNormLstm, useCudnnLstm=useCudnnLstm, viewActivations=True, useDenseLstm=useDenseLstm)
    else:
        forwardNetworkImagePlaceholder = tf.placeholder(tf.uint8, shape=(2, CROP_SIZE, CROP_SIZE, 3))

        # GRU
        if useGru:
            prevState = tuple([tf.placeholder(tf.float32, shape=(1, LSTM_SIZE)) for _ in range(2)])
        # res LSTM
        elif useResLstm:
            prevState = tuple([tf.placeholder(tf.float32, shape=(1, RES_LSTM_SIZE)) for _ in range(12)])
        # dense LSTM
        elif useDenseLstm:
            prevState = tuple([tf.placeholder(tf.float32, shape=(1, DENSE_LSTM_SIZE)) for _ in range(8)])
        # re3
        else:
            prevState = tuple([tf.placeholder(tf.float32, shape=(1, LSTM_SIZE)) for _ in range(4)])

        networkOutputs, fc6, lstmOuts, lstmStates = network.inference( #state1, state2 = network.inference(
                forwardNetworkImagePlaceholder, num_unrolls=1, train=False,
                prevState=prevState, reuse=False, trainCnn=trainCnn, useGru=useGru, useResLstm=useResLstm, useLayerNormLstm=useLayerNormLstm, useCudnnLstm=useCudnnLstm, viewActivations=True, useDenseLstm=useDenseLstm)

    tf_dataset_obj = tf_dataset.Dataset(sess, delta, batchSize * 2, PORT, useNetworkProb,
            debug=FLAGS.debug, useResLstm=useResLstm, useGru=useGru, useDenseLstm=useDenseLstm)
    #print "useGru: ", useGru, "prevstate: ", prevState
    tf_dataset_obj.initialize_tf_placeholders(
            forwardNetworkImagePlaceholder, prevState, networkOutputs, lstmStates)#state1, state2)


    tf_dataset_iterator = tf_dataset_obj.get_dataset(batchSize)
    imageBatch, labelsBatch = tf_dataset_iterator.get_next()
    imageBatch = tf.reshape(imageBatch, (batchSize * delta * 2, CROP_SIZE, CROP_SIZE, 3))
    labelsBatch = tf.reshape(labelsBatch, (batchSize * delta, -1))

    learningRate = tf.placeholder(tf.float32)
    imagePlaceholder = tf.placeholder(tf.uint8, shape=(batchSize, delta * 2, CROP_SIZE, CROP_SIZE, 3))
    labelPlaceholder = tf.placeholder(tf.float32, shape=(batchSize, delta, 4))

    if ',' in FLAGS.cuda_visible_devices:
        with tf.device('/gpu:0'):
            tfOutputs, fc6, lstmOuts = network.inference(imageBatch, num_unrolls=delta, train=True, reuse=True, trainCnn=trainCnn, useGru=useGru, useResLstm=useResLstm, useLayerNormLstm=useLayerNormLstm, useCudnnLstm=useCudnnLstm, viewActivations=True, useDenseLstm=useDenseLstm)
            tfLossFull, tfLoss, maeLoss = network.loss(tfOutputs, labelsBatch)
            train_op = network.training(tfLossFull, learning_rate)
    else:
        tfOutputs, fc6, lstmOuts = network.inference(imageBatch, num_unrolls=delta, train=True, reuse=True, trainCnn=trainCnn, useGru=useGru, useResLstm=useResLstm, useLayerNormLstm=useLayerNormLstm, useCudnnLstm=useCudnnLstm, viewActivations=True, useDenseLstm=useDenseLstm)
        tfLossFull, tfLoss, maeLoss = network.loss(tfOutputs, labelsBatch)
        train_op = network.training(tfLossFull, learning_rate)

    loss_summary_op = tf.summary.merge([
        tf.summary.scalar('MAE loss', maeLoss),
        tf.summary.scalar('loss', tfLoss),
        tf.summary.scalar('l2_regularizer', tfLossFull - tfLoss),
        ])

    if trainCnn:
        train_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
    else:
        train_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES)
    init = tf.global_variables_initializer()
    saver = tf.train.Saver()
    longSaver = tf.train.Saver()

    # Initialize the network and load saved parameters.
    sess.run(init)
    startIter = 0
    if FLAGS.restore:
        print('Restoring')
        startIter = tf_util.restore_from_dir(sess, os.path.join(LOG_DIR, 'checkpoints'))
    if not debug:
        tt = time.localtime()
        time_str = ('%04d_%02d_%02d_%02d_%02d_%02d' %
                (tt.tm_year, tt.tm_mon, tt.tm_mday, tt.tm_hour, tt.tm_min, tt.tm_sec))
        summary_writer = tf.summary.FileWriter(LOG_DIR + '/train/' + time_str +
                '_n_' + str(delta) + '_b_' + str(batchSize) + '_lr_' + str(learning_rate) + '_np_' + str(useNetworkProb), sess.graph)

        # added by Alessandro: visualize weight histogram for trainable vars
        for var in tf.trainable_variables():
            tf.summary.histogram(var.name, var)

        # add summaries to visualize activations of fc6 & resLstm blocks
        tf.summary.histogram('fc6', fc6)
        for i in range(len(lstmOuts)):
            tf.summary.histogram('resLstm' + str(i), lstmOuts[i])

        # add summaries to visualize gradients
        def log_gradients():

            with tf.name_scope('gradients'):
                gr = tf.get_default_graph()
                tensorNames= ['re3/fc_output/W_fc:0',
                              're3/fc6/W_fc:0']
                              #'re3/resBlock0/lstm0/rnn/lstm_cell/kernel:0',
                              #'re3/resBlock0/lstm1/rnn/lstm_cell/kernel:0',
                              #'re3/resBlock2/lstm4/rnn/lstm_cell/kernel:0',
                              #'re3/resBlock2/lstm5/rnn/lstm_cell/kernel:0']
                for tensorName in tensorNames:
                    weight = gr.get_tensor_by_name(tensorName)
                    grad = tf.gradients(tfLoss, weight)[0]
                    mean = tf.reduce_mean(tf.abs(grad))
                    tf.summary.scalar('mean_{}'.format(tensorName), mean)
                    tf.summary.histogram('histogram_{}'.format(tensorName), grad)
                    tf.summary.histogram('hist_weights_{}'.format(tensorName), grad)
        log_gradients()

        summary_full = tf.summary.merge_all()
        conv_var_list = [v for v in tf.trainable_variables() if 'conv' in v.name and 'weight' in v.name and
                (v.get_shape().as_list()[0] != 1 or v.get_shape().as_list()[1] != 1)]
        for var in conv_var_list:
            tf_util.conv_variable_summaries(var, scope=var.name.replace('/', '_')[:-2])

        summary_with_images = tf.summary.merge_all()

    # Logging stuff
    robustness_ph = tf.placeholder(tf.float32, shape=[])
    lost_targets_ph = tf.placeholder(tf.float32, shape=[])
    mean_iou_ph = tf.placeholder(tf.float32, shape=[])
    avg_ph = tf.placeholder(tf.float32, shape=[])
    if FLAGS.run_val:
        val_gpu = None if FLAGS.val_device == '0' else FLAGS.val_device
        test_tracker = re3_tracker.CopiedRe3Tracker(sess, train_vars, trainCnn, useGru, val_gpu, useResLstm=useResLstm, useLayerNormLStm=useLayerNormLstm)
        test_runner = test_net.TestTrackerRunner(test_tracker)
        with tf.name_scope('test'):
            test_summary_op = tf.summary.merge([
                tf.summary.scalar('robustness', robustness_ph),
                tf.summary.scalar('lost_targets', lost_targets_ph),
                tf.summary.scalar('mean_iou', mean_iou_ph),
                tf.summary.scalar('avg_iou_robustness', avg_ph),
                ])

    if debug:
        cv2.namedWindow('debug', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('debug', OUTPUT_WIDTH, OUTPUT_HEIGHT)

    sess.graph.finalize()


    def printVars():
        print '\nTrainable vars (', len(tf.trainable_variables()), '):'
        totTrainParams = 0
        for var in tf.trainable_variables():

            shape = var.get_shape()
            variable_parameters = 1
            for dim in shape:
                variable_parameters *= dim.value
            print '\t', var.name, ' shape: ', shape, ' params: ', variable_parameters

            totTrainParams += variable_parameters
        print 'Tot trainable params: ', totTrainParams

        print 'Non trainable vars (', len(tf.global_variables()) - len(tf.trainable_variables()), '):'
        for var in tf.global_variables():
            if var not in tf.trainable_variables():
                print '\t', var.name
        print '\n'

    # print trainable vars & number of parameters
    printVars()
    #exit(1)

    try:
        timeTotal = 0.000001
        numIters = 0
        iteration = startIter

        # Run training iterations in the main thread.
        while iteration < FLAGS.max_steps:
            if (iteration - 1) % 10 == 0:
                currentTimeStart = time.time()

            startSolver = time.time()
            if debug:
                _, outputs, lossValue, images, labels, = sess.run([
                    train_op, tfOutputs, tfLoss, imageBatch, labelsBatch],
                    feed_dict={learningRate : learning_rate})
                debug_feed_dict = {
                        imagePlaceholder : images,
                        labelPlaceholder : labels,
                        }
            else:
                if iteration % 10 == 0:
                    _, lossValue, loss_summary = sess.run([
                            train_op, tfLoss, loss_summary_op],
                            feed_dict={learningRate : learning_rate})
                    summary_writer.add_summary(loss_summary, iteration)
                else:
                    _, lossValue = sess.run([train_op, tfLoss],
                            feed_dict={learningRate : learning_rate})
            endSolver = time.time()

            numIters += 1
            iteration += 1

            timeTotal += (endSolver - startSolver)
            if timing and (iteration - 1) % 10 == 0:
                print('Iteration:       %d' % (iteration - 1))
                print('Loss:            %.3f' % lossValue)
                print('Average Time:    %.3f' % (timeTotal / numIters))
                print('Current Time:    %.3f' % (endSolver - startSolver))
                if numIters > 20:
                    print('Current Average: %.3f' % ((time.time() - currentTimeStart) / 10))
                print('')

            # Save a checkpoint and remove old ones.
            if iteration % 500 == 0 or iteration == FLAGS.max_steps:
                checkpoint_file = os.path.join(LOG_DIR, 'checkpoints', 'model.ckpt')
                saver.save(sess, checkpoint_file, global_step=iteration)
                if FLAGS.clearSnapshots:
                    files = glob.glob(LOG_DIR + '/checkpoints/*')
                    for file in files:
                        basename = os.path.basename(file)
                        if os.path.isfile(file) and str(iteration) not in file and 'checkpoint' not in basename:
                            os.remove(file)
            # Every once in a while save a checkpoint that isn't ever removed except by hand.
            # skip because of drive space
            '''if iteration % 10000 == 0 or iteration == FLAGS.max_steps:
                if not os.path.exists(LOG_DIR + '/checkpoints/long_checkpoints'):
                    os.makedirs(LOG_DIR + '/checkpoints/long_checkpoints')
                checkpoint_file = os.path.join(LOG_DIR, 'checkpoints/long_checkpoints', 'model.ckpt')
                longSaver.save(sess, checkpoint_file, global_step=iteration)'''
            if not debug:
                if (numIters == 1 or
                    iteration % 200 == 0 or
                    iteration == FLAGS.max_steps):
                    # Write out the full graph sometimes.
                    if (numIters == 1 or
                        iteration == FLAGS.max_steps):
                        print('Running detailed summary')
                        run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
                        run_metadata = tf.RunMetadata()
                        _, summary_str = sess.run([train_op, summary_with_images],
                                              options=run_options,
                                              run_metadata=run_metadata,
                                              feed_dict={learningRate : learning_rate})
                        summary_writer.add_run_metadata(run_metadata, 'step_%07d' % iteration)

                    elif iteration % 2000 == 0:
                        _, summary_str = sess.run([train_op, summary_with_images],
                            feed_dict={learningRate : learning_rate})
                        print('Running image summary')

                    else:
                        print('Running summary')
                        _, summary_str = sess.run([train_op, summary_full],
                            feed_dict={learningRate : learning_rate})
                    summary_writer.add_summary(summary_str, iteration)
                    summary_writer.flush()

                if FLAGS.run_val and (numIters == 1 or iteration % 500 == 0):
                    # Run a validation set eval in a separate thread.
                    def test_func(test_iter_on):
                        print('Starting test iter', test_iter_on)
                        print "--------------->TEST ITER"
                        test_runner.reset()  # causes exceptions in other concurrent validation threads since resets the list of the filenames used for validation
                        result = test_runner.run_test(dataset=FLAGS.val_dataset, display=False)
                        summary_str = sess.run(test_summary_op, feed_dict={
                            robustness_ph : result['robustness'],
                            lost_targets_ph : result['lostTarget'],
                            mean_iou_ph : result['meanIou'],
                            avg_ph : (result['meanIou'] + result['robustness']) / 2,
                            })
                        summary_writer.add_summary(summary_str, test_iter_on)
                        summary_writer.flush()
                        os.remove('results.json')
                        print('Ending test iter', test_iter_on)
                    test_thread = threading.Thread(target=test_func, args=(iteration,))
                    test_thread.start()
            if FLAGS.output:
                # Look at some of the outputs.
                print('new batch')
                images = debug_feed_dict[imagePlaceholder].astype(np.uint8).reshape(
                        (batchSize, delta, 2, CROP_SIZE, CROP_SIZE, 3))
                labels = debug_feed_dict[labelPlaceholder].reshape(
                        (batchSize, delta, 4))
                outputs = outputs.reshape((batchSize, delta, 4))
                for bb in range(batchSize):
                    for dd in range(delta):
                        image0 = images[bb,dd,0,...]
                        image1 = images[bb,dd,1,...]

                        label = labels[bb,dd,:]
                        xyxyLabel = label / 10
                        labelBox = xyxyLabel * CROP_PAD

                        output = outputs[bb,dd,...]
                        xyxyPred = output / 10
                        outputBox = xyxyPred * CROP_PAD

                        drawing.drawRect(image0, bb_util.xywh_to_xyxy(np.full((4,1), .5) * CROP_SIZE), 2, [255,0,0])
                        drawing.drawRect(image1, xyxyLabel * CROP_SIZE, 2, [0,255,0])
                        drawing.drawRect(image1, xyxyPred * CROP_SIZE, 2, [255,0,0])

                        plots = [image0, image1]
                        subplot = drawing.subplot(plots, 1, 2, outputWidth=OUTPUT_WIDTH, outputHeight=OUTPUT_HEIGHT, border=5)
                        cv2.imshow('debug', subplot[:,:,::-1])
                        cv2.waitKey(0)
    except:
        # Save if error or killed by ctrl-c.
        if not debug:
            print('Saving...')
            checkpoint_file = os.path.join(LOG_DIR, 'checkpoints', 'model.ckpt')
            saver.save(sess, checkpoint_file, global_step=iteration)
        raise

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Training for Re3.')
    parser.add_argument('-n', '--num_unrolls', action='store', default=2, dest='delta', type=int)
    parser.add_argument('-b', '--batch_size', action='store', default=64, type=int)
    parser.add_argument('-v', '--cuda_visible_devices', type=str, default=str(GPU_ID), help='Device number or string')
    parser.add_argument('-r', '--restore', action='store_true', default=False)
    parser.add_argument('-d', '--debug', action='store_true', default=False)
    parser.add_argument('-t', '--timing', action='store_true', default=False)
    parser.add_argument('-o', '--output', action='store_true', default=False)
    parser.add_argument('-c', '--clear_snapshots', action='store_true', default=False, dest='clearSnapshots')
    parser.add_argument('-p', '--port', action='store', default=9997, dest='port', type=int)
    parser.add_argument('--run_val', action='store_true', default=False)
    parser.add_argument('--val_dataset', type=str, default='imagenet_video', help='Dataset to test on.') # previously default='vot'
    parser.add_argument('--val_device', type=str, default='0', help='Device number or string for val process to use.')
    parser.add_argument('-m', '--max_steps', type=int, default=NUM_ITERATIONS, help='Number of steps to run trainer.')

    # added by Alessandro
    parser.add_argument('--fix_cnn', action='store_true', default=False, help='Fix the cnn weights')     # fix cnn weights. DEFAULT = False
    parser.add_argument('--use_gru', action='store_true', default=False, help='Use GRU instead of LSTM') # use GRU instead of LSTM. DEFAULT = False
    parser.add_argument('-lr', '--learning_rate', type=float, default=1e-5, help='Set the learning rate')
    parser.add_argument('-np', '--network_prob', type=float, default=0, help='Probability of using network predictions as training data')
    parser.add_argument('--res_lstm', action='store_true', default=False, help='Use stacked residual LSTMs instead of standard Re3 2 stacked LSTMs')
    parser.add_argument('--layer_norm_lstm', action='store_true', default=False, help='Use layer normalization on lstm')
    parser.add_argument('--cudnn_lstm', action='store_true', default=False, help='Use cudnn implementation of the lstm')
    parser.add_argument('--dense_lstm', action='store_true', default=False, help='Use densely connected LSTMs')

    FLAGS = parser.parse_args()
    main(FLAGS)

